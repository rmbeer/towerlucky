# TowerLucky

**For KrampusHack 2021**

## DESCRIPTION:

Apparently I couldn't think of anything creative for my first game. So I had to choose a different one in order to give GullRaDriel a gift.

I let you intuit about the rules of the game. If you clear all the pieces, consider that you have won, otherwise, if you cannot advance, then you have lost.

The game also has a hidden secret.

## ABOUT OF SOURCE CODE:

Built with C++ using C code and Allegro 5 library.

based on Orchid Adventure, replace almost all the code, so you could find useless residue from the previous game.

A game that has consumed me 4 hours of programming. And 1 hour of testing.

Why did I do it so fast this time? Because it didn't require inspiration or creativity.

No sound and no animations.

## IMAGE:

![ScreenShot](https://0x0.st/os3q.png)

## DOWNLOAD:
[ From TINS Site ](https://tins.amarillion.org/upload/TowerLucky.tar.xz)

Only source codes, tested only in GNU/Linux.

## INSTRUCTIONS:
### For tarball:
<pre>tar Jxvf TowerLucky.tar.xz
cd TowerLucky</pre>

### For bitbucket:
<pre>git clone https://bitbucket.org/rmbeer/towerlucky TowerLucky
cd TowerLucky</pre>

### For compiling from both sources:
<pre>mkdir Build
cd Build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
ln -s ../Datos
./main</pre>

Juego creado para KrampusHack 2021 (31 de Diciembre)

### LICENCIAS:

El codigo fuente tiene licencia GPL y los archivos de datos tiene CC (BY-NC-SA).

