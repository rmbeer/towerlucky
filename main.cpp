#include <stdio.h>
#include "BaseBmp.h"

//#define MUSIC

ALLEGRO_DISPLAY*dsp;
int CICLO,k;
ALLEGRO_BITMAP*scr,*fnd;
ALLEGRO_FONT*fnt,*fntnum;
ALLEGRO_TIMER*timer;
ALLEGRO_EVENT_QUEUE*evq;
int tfnt;
int fndtx,fndty;
int SCRTX,SCRTY;
int mx,my,cmx,cmy,cbfch;
int alma[12];

#ifdef MUSIC
ALLEGRO_VOICE*voice;
ALLEGRO_MIXER*mixer;
ALLEGRO_AUDIO_STREAM*stream;
#endif

//Fichas
struct _STR_fch{
  int px,py; //Posicion grafico
  int num; //Numero
  int col; //Color
  int hid; //Oculto
  int nxt; //Siguiente en stack
}fch[156];

//============================================================================//
// FUNCIONES GENERALES                                                        //
//============================================================================//

ALLEGRO_COLOR pal[256];

void SetColorRawRGB(ALLEGRO_COLOR*zp,long zc){
  *zp=al_map_rgba((zc>>16)&255,(zc>>8)&255,zc&255,255);
}

void SetCPal(){
  long LC[16]={
    0x101010, //negro         0
    0xffffff, //blanco        1
    0xe04040, //rojo          2
    0x60ffff, //cian          3
    0xe060e0, //violeta       4
    0x40e040, //verde         5
    0x4040e0, //azul          6
    0xffff40, //amarillo      7
    0xe0a040, //naranja       8
    0x9c7448, //marron        9
    0xffa0a0, //rojo claro    a
    0x545454, //gris oscuro   b
    0x888888, //medio gris    c
    0xa0ffa0, //verde claro   d
    0xa0a0ff, //azul claro    e
    0xc0c0c0}; //gris claro    f
  for(int i=0;i<16;i++)
    SetColorRawRGB(pal+i,LC[i]);
}

int InitScreen(){int err=1;
  if(!al_init())return err;err++;
  if(!al_install_keyboard())return err;err++;
  if(!al_install_mouse())return err;err++;
  /* TODO: Revisar si para las 'VOICE' necesita ser unico para todas las ventanas */
  #ifdef MUSIC
  if(!al_install_audio())return err;err++;
  if(!al_init_acodec_addon())return err;err++;
  #endif
  if(!al_init_image_addon())return err;err++;
  if(!al_init_primitives_addon())return err;err++;
  al_init_font_addon();
  if(!al_init_ttf_addon())return err;err++;
  evq=al_create_event_queue();
  if(!evq)return err;err++;
  timer=al_create_timer(1.0/60);
  if(!timer)return err;err++;
  #ifdef MUSIC
  voice=al_create_voice(44100,ALLEGRO_AUDIO_DEPTH_INT16,ALLEGRO_CHANNEL_CONF_2);
  if(!voice)return err;err++;
  mixer=al_create_mixer(44100,ALLEGRO_AUDIO_DEPTH_FLOAT32,ALLEGRO_CHANNEL_CONF_2);
  if(!mixer)return err;err++;
  if(!al_attach_mixer_to_voice(mixer, voice))return err;err++;
  stream=al_load_audio_stream("./Datos/msc/armanis_dreams.mod",4,2048);
  if(!stream)return err;err++;
  #endif
  al_register_event_source(evq,al_get_timer_event_source(timer));
  al_register_event_source(evq,al_get_keyboard_event_source());
  al_register_event_source(evq,al_get_mouse_event_source());
  //al_set_new_display_flags(ALLEGRO_FRAMELESS);
  al_set_new_display_flags(ALLEGRO_RESIZABLE|ALLEGRO_MAXIMIZED);
  dsp=al_create_display(640,480);
  if(!dsp)return err;err++; //9
  al_register_event_source(evq,al_get_display_event_source(dsp));
  al_set_blender(ALLEGRO_ADD,ALLEGRO_ALPHA,ALLEGRO_INVERSE_ALPHA);
  scr=al_create_bitmap(320,240);
  if(!scr)return err;err++;
  fnd=al_load_bitmap("./Datos/img/fondo.jpg");
  if(!fnd)return err;err++;
  fnt=al_load_ttf_font("./Datos/fnt/BubblegumSans-Regular.ttf",16,ALLEGRO_TTF_MONOCHROME);
  if(!fnt)return err;err++; //16
  {ALLEGRO_BITMAP*gnum;
    gnum=al_load_bitmap("./Datos/img/Numeros.png");
    if(!gnum)return err;err++; //17
    int zrang[]={64,76};
    fntnum=al_grab_font_from_bitmap(gnum,1,zrang);
  }
  tfnt=al_get_font_line_height(fnt);
  SetCPal();
  al_start_timer(timer);
  SCRTX=al_get_display_height(dsp);SCRTY=al_get_display_width(dsp);
  return 0;
}

void EndScreen(){
  #ifdef MUSIC
  al_destroy_audio_stream(stream);
  #endif
  al_uninstall_system();
}

/** Entrada de teclado */
void EvKey(ALLEGRO_EVENT&ev){
  switch(ev.type){
  case ALLEGRO_EVENT_KEY_DOWN:
    switch(ev.keyboard.keycode){
    case ALLEGRO_KEY_RIGHT:k|=1;break;
    case ALLEGRO_KEY_UP:k|=2;break;
    case ALLEGRO_KEY_LEFT:k|=4;break;
    case ALLEGRO_KEY_DOWN:k|=8;break;
    case ALLEGRO_KEY_Z:k|=16;break;
    case ALLEGRO_KEY_X:k|=32;break;
    case ALLEGRO_KEY_C:k|=64;break;
    case ALLEGRO_KEY_A:k|=128;break;
    case ALLEGRO_KEY_S:k|=256;break;
    case ALLEGRO_KEY_D:k|=512;break;
    case ALLEGRO_KEY_ENTER:k|=1024;break;
    case ALLEGRO_KEY_RSHIFT:k|=1048;break;
    case ALLEGRO_KEY_F5:k|=0x80000000;break;
    case ALLEGRO_KEY_ESCAPE:CICLO=0;break;
    //case ALLEGRO_KEY_Q:printf("PUTAO\n");break;
    }break;
  case ALLEGRO_EVENT_KEY_UP:
    switch(ev.keyboard.keycode){
    case ALLEGRO_KEY_RIGHT:k&=~1;break;
    case ALLEGRO_KEY_UP:k&=~2;break;
    case ALLEGRO_KEY_LEFT:k&=~4;break;
    case ALLEGRO_KEY_DOWN:k&=~8;break;
    case ALLEGRO_KEY_Z:k&=~16;break;
    case ALLEGRO_KEY_X:k&=~32;break;
    case ALLEGRO_KEY_C:k&=~64;break;
    case ALLEGRO_KEY_A:k&=~128;break;
    case ALLEGRO_KEY_S:k&=~256;break;
    case ALLEGRO_KEY_D:k&=~512;break;
    case ALLEGRO_KEY_ENTER:k&=~1024;break;
    case ALLEGRO_KEY_RSHIFT:k&=~1048;break;
    case ALLEGRO_KEY_F5:k&=~0x80000000;break;
    }break;
  //case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
  //case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
  case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
    k|=0x80000000;cmx=ev.mouse.y;cmy=ev.mouse.x;
    break;
  case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
    k|=0x40000000;mx=ev.mouse.y;my=ev.mouse.x;
    break;
  case ALLEGRO_EVENT_MOUSE_AXES:
    mx=ev.mouse.y;my=ev.mouse.x;
    break;
  case ALLEGRO_EVENT_DISPLAY_RESIZE:
    SCRTX=ev.display.height;SCRTY=ev.display.width;
    al_acknowledge_resize(ev.display.source);
    break;
  }
}

//============================================================================//
// FUNCIONES JUEGO                                                            //
//============================================================================//

int(*Ejec)()=0;

int EGame();
int IEGame(){int ax,ay,az=0;
  al_stop_timer(timer);
  fndtx=al_get_bitmap_height(fnd);
  fndty=al_get_bitmap_width(fnd);
  //Prepara fichas
  for(ax=0;ax<12;ax++)alma[ax]=0;
  //Genera numeros
  for(ax=0;ax<13;ax++)for(ay=0;ay<3;ay++){
    fch[az].num=ax;fch[az].col=0;az++;
    fch[az].num=ax;fch[az].col=1;az++;
    fch[az].num=ax;fch[az].col=2;az++;
    fch[az].num=ax;fch[az].col=3;az++;
  }
  //Mezcla
  for(ax=0;ax<156;ax++){
    _STR_fch zaux;
    ay=rand()%156;
    zaux=fch[ax];
    fch[ax]=fch[ay];
    fch[ay]=zaux;
  }az=0;
  //Corta y oculta
  for(ax=0;ax<13;ax++){
    for(ay=0;ay<ax;ay++){
      fch[az].hid=1;fch[az].px=ay+1;fch[az].py=ax;fch[az].nxt=az+1;az++;
    }
    for(ay=0;ay<6;ay++){
      fch[az].hid=0;fch[az].px=ax+ay+1;fch[az].py=ax;if(ay==5)fch[az].nxt=-1;else fch[az].nxt=az+1;az++;
    }
  }
  //Continua la inicializacion
  #ifdef MUSIC
  al_set_audio_stream_playmode(stream,ALLEGRO_PLAYMODE_LOOP);
  al_register_event_source(evq,al_get_audio_stream_event_source(stream));
  if(!al_attach_audio_stream_to_mixer(stream,mixer))return -1;
  //if(!al_attach_audio_stream_to_voice(stream,voice))return -1;
  #endif
  Ejec=EGame;
  al_set_target_bitmap(scr);
  al_clear_to_color(pal[0]);
  al_resume_timer(timer);
  return 0;
}

void DrawGame(){int ix,iy,zx,zy;float tmpy,tmpx;
  //PANTALLA
  //al_set_target_bitmap(scr);
  //al_clear_to_color(al_map_rgba(0,0,0,0));
  //al_draw_bitmap(fnd,0,0,0);
  //RENDER
  al_set_target_backbuffer(dsp);
  //  al_draw_scaled_bitmap(scr,0,0,320,240,0,0,640,480,0);
  //al_draw_bitmap(fnd,0,0,0);
  tmpy=SCRTY/13.0;
  al_draw_scaled_bitmap(fnd,0,0,fndty,fndtx,0,0,SCRTY,SCRTX,0);
  //for(ix=0;ix<10;ix++)for(iy=0;iy<13;iy++){
  //  al_draw_filled_rectangle(iy*tmpy,SCRTX-ix*16,(iy+1)*tmpy,SCRTX-ix*16+16,pal[11]);
  //  al_draw_rectangle(iy*tmpy,SCRTX-ix*16,(iy+1)*tmpy,SCRTX-ix*16+16,pal[12],0);
  //}
  for(ix=0;ix<156;ix++){
    if(fch[ix].hid&4)continue;
    zx=SCRTX-fch[ix].px*16;zy=fch[ix].py*tmpy;
    if(fch[ix].hid&2){zx-=cmx-mx;zy-=cmy-my;}
    if(fch[ix].hid&1){
      al_draw_filled_rectangle(zy,zx,zy+tmpy,zx+16,pal[11]);
      al_draw_rectangle(zy,zx,zy+tmpy,zx+16,pal[12],0);
    }else{
      //Bloques colores: 5d-verde 2a-rojo 6e-azul 98-dorado
      switch(fch[ix].col){
      case 0:
        al_draw_filled_rectangle(zy,zx,zy+tmpy,zx+16,pal[5]);
        al_draw_rectangle(zy+3,zx+3,zy+tmpy-3,zx+14,pal[13],3);
        al_draw_glyph(fntnum,al_map_rgb(255,0,255),zy+tmpy/2-12,zx,fch[ix].num+64);
        break;
      case 1:
        al_draw_filled_rectangle(zy,zx,zy+tmpy,zx+16,pal[2]);
        al_draw_rectangle(zy+3,zx+3,zy+tmpy-3,zx+14,pal[10],3);
        al_draw_glyph(fntnum,al_map_rgb(255,255,0),zy+tmpy/2-12,zx,fch[ix].num+64);
        break;
      case 2:
        al_draw_filled_rectangle(zy,zx,zy+tmpy,zx+16,pal[6]);
        al_draw_rectangle(zy+3,zx+3,zy+tmpy-3,zx+14,pal[14],3);
        al_draw_glyph(fntnum,al_map_rgb(255,255,255),zy+tmpy/2-12,zx,fch[ix].num+64);
        break;
      case 3:
        al_draw_filled_rectangle(zy,zx,zy+tmpy,zx+16,pal[9]);
        al_draw_rectangle(zy+3,zx+3,zy+tmpy-3,zx+14,pal[8],3);
        al_draw_glyph(fntnum,al_map_rgb(0,0,255),zy+tmpy/2-12,zx,fch[ix].num+64);
        break;
      }
    }
  }
  //
  for(ix=0;ix<12;ix++){
    zy=(ix&3)*27+SCRTY/2.0-54;zx=(ix&0xc)<<2;
    //Bloques colores: 5d-verde 2a-rojo 6e-azul 98-dorado
    switch(ix&3){
    case 0:
      al_draw_filled_rectangle(zy,zx,zy+27,zx+16,pal[5]);
      al_draw_rectangle(zy+3,zx+3,zy+24,zx+14,pal[13],3);
      if(alma[ix])al_draw_glyph(fntnum,al_map_rgb(255,0,255),zy+1,zx,alma[ix]+63);
      break;
    case 1:
      al_draw_filled_rectangle(zy,zx,zy+27,zx+16,pal[2]);
      al_draw_rectangle(zy+3,zx+3,zy+24,zx+14,pal[10],3);
      if(alma[ix])al_draw_glyph(fntnum,al_map_rgb(255,255,0),zy+1,zx,alma[ix]+63);
      break;
    case 2:
      al_draw_filled_rectangle(zy,zx,zy+27,zx+16,pal[6]);
      al_draw_rectangle(zy+3,zx+3,zy+24,zx+14,pal[14],3);
      if(alma[ix])al_draw_glyph(fntnum,al_map_rgb(255,255,255),zy+1,zx,alma[ix]+63);
      break;
    case 3:
      al_draw_filled_rectangle(zy,zx,zy+27,zx+16,pal[9]);
      al_draw_rectangle(zy+3,zx+3,zy+24,zx+14,pal[8],3);
      if(alma[ix])al_draw_glyph(fntnum,al_map_rgb(0,0,255),zy+1,zx,alma[ix]+63);
      break;
    }
  }
  al_flip_display();
}

int EGame(){int ix,iy,zx,zy,zz;float tmpy,tmpx;
  tmpy=SCRTY/13.0;
  switch((k>>28)&0xc){
  case 0x8:
    ix=(SCRTX-cmx)/16+1;iy=cmy/tmpy;
    for(zz=0;zz<156;zz++){if(fch[zz].hid&5)continue;
      if(ix==fch[zz].px&&iy==fch[zz].py){cbfch=zz;
        for(zx=zz;fch[zx].nxt!=-1;zx=fch[zx].nxt)fch[zx].hid|=2;fch[zx].hid|=2;
        zz=156;
      }
    }
    break;
  case 0xc:
    k&=~0xc0000000;
    ix=(SCRTX-mx)/16+1;iy=my/tmpy;zy=-1;
    if(iy!=fch[cbfch].py){
      if(fch[cbfch].num==12){
        for(zz=0;zz<156;zz++){if(fch[zz].hid&4)continue;
          if(iy==fch[zz].py)zy=zz;}
        if(zy==-1){ix=0;
          for(zz=0;zz<156;zz++){if(fch[zz].hid&4)continue;
            if(fch[zz].nxt==cbfch){fch[zz].nxt=-1;
              if(fch[zz].hid&1)fch[zz].hid&=~1;break;}}
          for(zx=cbfch;fch[zx].nxt!=-1;zx=fch[zx].nxt){fch[zx].py=iy;fch[zx].px=1+ix++;}
          fch[zx].py=iy;fch[zx].px=1+ix++;
        }
      }else{
        //printf("M: %d - %d / %d - %d / %d - %d\n",cmx,cmy,mx,my,ix,iy);
        for(zz=0;zz<156;zz++){if(fch[zz].hid&4)continue;
          if(iy==fch[zz].py&&(zy==-1||fch[zz].px>fch[zy].px))zy=zz;}
        if(zy!=-1){
          //printf("%d - %d / %d - %d\n",fch[cbfch].num,fch[zy].num,fch[cbfch].col,fch[zy].col);
          if(fch[cbfch].num+1==fch[zy].num&&fch[cbfch].col==fch[zy].col){
            for(zz=0;zz<156;zz++){if(fch[zz].hid&4)continue;
              if(fch[zz].nxt==cbfch){fch[zz].nxt=-1;
                if(fch[zz].hid&1)fch[zz].hid&=~1;break;}}
            fch[zy].nxt=cbfch;
            for(;fch[zy].nxt!=-1;zy=fch[zy].nxt){
              fch[fch[zy].nxt].px=fch[zy].px+1;fch[fch[zy].nxt].py=fch[zy].py;
            }
          }
        }
      }
    }for(zx=cbfch;fch[zx].nxt!=-1;zx=fch[zx].nxt)fch[zx].hid&=~2;fch[zx].hid&=~2;
    //Guarda todo en el almacen
    for(zz=0;zz<156;zz++)if(!(fch[zz].hid&4)&&fch[zz].nxt==-1){
      if(alma[fch[zz].col]==fch[zz].num){
        alma[fch[zz].col]++;for(zy=0;zy<156;zy++)if(fch[zy].nxt==zz)break;
        if(zy<156){fch[zy].nxt=-1;fch[zy].hid&=~1;}fch[zz].hid|=4;zz=-1;
      }else if(alma[fch[zz].col+4]==fch[zz].num){
        alma[fch[zz].col+4]++;for(zy=0;zy<156;zy++)if(fch[zy].nxt==zz)break;
        if(zy<156){fch[zy].nxt=-1;fch[zy].hid&=~1;}fch[zz].hid|=4;zz=-1;
      }else if(alma[fch[zz].col+8]==fch[zz].num){
        alma[fch[zz].col+8]++;for(zy=0;zy<156;zy++)if(fch[zy].nxt==zz)break;
        if(zy<156){fch[zy].nxt=-1;fch[zy].hid&=~1;}fch[zz].hid|=4;zz=-1;
      }
    }
    break;
  }
  DrawGame();
  return 0;
}

//============================================================================//
// CICLO                                                                      //
//============================================================================//

int Bucle(){int VIS=0;
  ALLEGRO_EVENT ev;
  do{
    do{
      al_wait_for_event(evq,&ev);
      switch(ev.type){
      case ALLEGRO_EVENT_DISPLAY_CLOSE:CICLO=0;return 0;
      case ALLEGRO_EVENT_TIMER:VIS=1;break;
      case ALLEGRO_EVENT_DISPLAY_SWITCH_OUT:break;
      case ALLEGRO_EVENT_DISPLAY_SWITCH_IN:break;
      case ALLEGRO_EVENT_KEY_CHAR:
      case ALLEGRO_EVENT_KEY_UP:
      case ALLEGRO_EVENT_KEY_DOWN:
      case ALLEGRO_EVENT_MOUSE_AXES:
      case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
      case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
      case ALLEGRO_EVENT_DISPLAY_RESIZE:
        EvKey(ev);break;
      }
    }while(!al_event_queue_is_empty(evq));
    if(VIS){
      if(Ejec())CICLO=0;
      VIS=0;
    }
  }while(CICLO);
}

int main(){int i;
  if(i=InitScreen()){printf("ERROR:%d\n",i);return -1;}
  Ejec=IEGame;CICLO=1;
  al_start_timer(timer);
  srand(time(0));
  Bucle();
  EndScreen();
  return 0;
}
